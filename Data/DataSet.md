# Data Sets

1. [Overview](#overview)
2. [Refining the Data Set](#refining-the-data-set)
3. [Improvement of Data Labeling](#improvement-of-data-labeling)

## Overview

The table below provides an overview of the different versions of our dataset used in the course of our computer vision project. For each version, details regarding pre-processing, the type of pre-processing implemented, data augmentation, and the distribution of data into training, validation, and testing sets are specified. Throughout the project, we trained multiple models using these datasets, refining our approach with each iteration. This table serves as a reference, documenting the evolution of the dataset from its initial iteration to the final version.

| Dataset Version | Pre Processing | Pre-Processing Type   | Augmented Data | Train/Val/Test Split | Link                                                  |
| --------------- | -------------- | --------------------- | -------------- | -------------------- | ----------------------------------------------------- |
| v1_0            | TRUE           | Auto-orient, 640x640​ | TRUE           | 12k/1.1k/659​        | https://app.roboflow.com/aisscv-ffp8r/aiss_cv-sx8yb/1 |
| v1_1            | TRUE           | Auto-orient, 640x640​ | FALSE          | 4.2k/1.2k/725        | https://app.roboflow.com/aisscv-ffp8r/aiss_cv-sx8yb/3 |
| v3_0/vfinal     | TRUE           | Auto-orient, 640x640​ | FALSE          | 4.9k/1.4k/837​       | https://app.roboflow.com/aisscv-ffp8r/aiss_cv-sx8yb/6 |

## Refining the Data Set

Our journey in refining the dataset was systematic and driven by iterative improvements. Here's a brief account of our process:

1. **Initial Collection**: In our first iteration, we generated around 6000 images. The presence of some parts in higher counts led to their overrepresentation, while some states, only being present once, ended up underrepresented. This posed a risk to the robustness of our dataset.
2. **Balancing the Imbalance**: To counteract this disparity, we proactively added more images for the underrepresented states. It wasn't just about quantity but ensuring diversity and representation.
3. **Performance Analysis**: Post-enhancement, a detailed performance check brought to light that state_9, state_10, and state_11 were lagging behind. While some false labels were a contributing factor, the primary challenge lay in the intrinsic similarities between these states. Their nuanced differences made it crucial to augment the dataset with more images of these specific states.
4. **Further Scrutiny**: Upon closer inspection, performance issues were also observed for state*12 and state_13. The almost indistinguishable nature of these states, separated solely by a \_gray_short*, demanded special attention. Recognizing this nuance, we supplemented the dataset with around 120 images for each state, underscoring their distinct features.

The process of dataset refinement was not merely about accumulation but was rooted in the tenets of diversity, representation, and accuracy. With each phase, we edged closer to a balanced and representative dataset. Our systematic and iterative method led to consistent enhancements, ensuring our dataset's alignment with the envisioned goals.

## Improvement of Data Labeling

The need for high-quality labeled data is very crucial. An integral aspect of this labeled data is the precision of bounding boxes that mark the objects of interest in images.

When we talk about precision in bounding boxes, we refer to how closely the box adheres to the edges of the object it encloses. A tight bounding box wraps closely around the object, with minimal space between the object's boundaries and the box itself. But why is this so critical, especially for YOLO models?

1. **Higher Intersection Over Union (IoU):** IoU is a popular metric used in object detection tasks to measure the accuracy of an object detector on a specific dataset. A higher IoU signifies that the predicted bounding box and the ground truth box have a larger overlap, and hence, the prediction is more accurate. Tight bounding boxes ensure that the ground truth is as precise as possible, leading to more accurate training and better model performance.
2. **Less Background Noise:** Loosely fitted bounding boxes inadvertently include parts of the background. This background noise can be misleading for the model during training, making it harder for the model to distinguish between the object and its surroundings. A tight bounding box reduces this noise, ensuring that the model focuses purely on the object.
3. **YOLO's Architecture:** YOLO divides an image into a grid and predicts bounding boxes and class probabilities for each grid cell. The model's loss function penalizes deviations between the predicted and actual bounding box coordinates. Tight bounding boxes ensure that the model learns the most accurate representation during training, which significantly improves detection during inference.
4. **Consistent Spatial Understanding:** By consistently training the model with tight bounding boxes, it gains a more refined spatial understanding of objects in different scenarios, orientations, and scales. This consistency in training data quality can lead to fewer false positives and more accurate detections in real-world scenarios.
5. **Optimized Performance:** Precision in labeling, especially with tight bounding boxes, often leads to a tangible increase in model performance. As experienced with the 7,150 images that were re-annotated, tightening bounding boxes can sometimes result in marked improvements in the model's ability to detect and classify objects accurately.

In conclusion, while the process of curating and refining labeled data might seem tedious, its importance cannot be overstated, especially for advanced object detection models like YOLO. Tight bounding boxes ensure that the model learns from the best possible representation of objects, enabling it to achieve superior performance during inference. In the ever-evolving field of computer vision, every ounce of accuracy counts, and as we learned, ensuring the quality of annotations can make a profound difference.

![v1_1](../assets/DataSet/v1_1.png)

Loose bounding boxes in v1_1

![v3_0](../assets/DataSet/v3_0.png)

Tightened bounding boxes in v3_0/vFinal
