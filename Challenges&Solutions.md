# Challenges & Solutions

1. [Jetson Nano](#jetson-nano)
   - [Image and Versioning](#image-and-versioning)
   - [Memory](#memory)
   - [Overheating](#overheating)
2. [Training resources](#training-resources)
3. [Increase Swap](#increase-swap)
4. [Time](#time)

## Jetson Nano

### Image and Versioning

During the course of our project, we encountered several roadblocks related to software dependencies, compatibility, and versioning, primarily on the Jetson Nano using NVIDIA's stock image. The interconnected nature of the tools we were using, coupled with the specific requirements of our setup, made maintaining a consistent environment challenging.

1. **Python Versions:** The world of Python has seen several versions, each with its own quirks and features. Given the breadth of our project and the libraries involved, aligning with a consistent Python version that was universally supported became critical. However, due to disparities in library support for different Python versions, this wasn't straightforward.
2. **PyTorch and CUDA:** PyTorch's dynamic computation graph is a boon for deep learning tasks, but ensuring it works seamlessly with NVIDIA’s CUDA for GPU acceleration was a task in itself. Different versions of PyTorch require specific versions of CUDA, and finding the right pairing that was compatible with other components of our system became a challenge.
3. **TensorRT Complications:** While TensorRT promises efficient neural network inference, integrating it proved difficult. The disparity between TensorRT's supported versions and the other libraries' requirements led to conflicts. This problem was exacerbated when considering the YOLOv8 inference, which we aimed to accelerate using TensorRT.
4. **Ultralytics and OpenCV:** Ultralytics has been pivotal in our object detection tasks, but ensuring its compatibility with OpenCV versions was another concern. OpenCV, given its extensive utility in image processing tasks, had to be a specific version that worked harmoniously with both Ultralytics and our other tools.
5. **NVIDIA's Stock Image:** The stock image provided by NVIDIA for the Jetson Nano, while robust, wasn’t tailored to our specific requirements. We faced a series of challenges, from installation errors to runtime conflicts, stemming from this image's inherent configurations and pre-installed packages.

The cumulative effect of these challenges threatened to impede our progress. However, our perseverance led us to a solution in the form of an image provided by Qengineering. Their **[Jetson Nano Ubuntu 20 image](https://github.com/Qengineering/Jetson-Nano-Ubuntu-20-image)** was a revelation. Not only did it come equipped with most of the tools and libraries we needed, but it also offered a more streamlined setup process, effectively sidestepping many of the aforementioned versioning and dependency conflicts. This ready-to-use image became the linchpin of our setup, allowing us to focus on the core aspects of our project rather than getting mired in configuration woes.

### Memory

The Jetson Nano, while designed to handle a myriad of computation-intensive tasks, is sometimes confronted with memory constraints. Possessing a limited RAM capacity, when subjected to large-scale machine learning models, extensive datasets, or multiple simultaneous applications, the Nano can encounter memory bottlenecks. Such shortages not only degrade performance, often leading to significant slowdowns, but can also result in unexpected application crashes or system instability. The limited memory also means developers need to be judicious in their resource allocation, often having to optimize models or processes specifically for deployment on the Nano. It's vital to implement efficient memory management practices and be wary of the potential pitfalls of running large or numerous applications without monitoring the device's memory usage.

### Overheating

The Jetson Nano, while renowned for its computational capabilities in a compact form factor, is not without challenges. One notable concern is overheating. The combination of its high-performance GPU, quad-core CPU, and the demanding tasks it's often assigned, such as machine learning inferencing, can result in substantial heat generation. Without adequate thermal management, the Jetson Nano can reach temperatures that may throttle its performance or, in extreme cases, risk hardware damage. Overheating can also introduce erratic behaviors in applications, compromising the reliability and predictability of solutions built on the platform. Consequently, users and developers working with the Jetson Nano often have to invest in additional cooling mechanisms, like heatsinks or fans, and monitor the device's temperature to ensure stable and sustained operation.

## Training resources

In the demanding landscape of machine learning, having access to robust computational resources, especially GPUs, is non-negotiable. These powerhouses play a pivotal role in expediting model training, promoting both efficiency and punctuality in project execution.

Within our team, we began our journey with two personal GPUs provided by team members. These resources were instrumental in initiating our training tasks, but as we sought to broaden our research, their limitations became evident.

Our ambition was to train multiple models using varied training sets to gain a comprehensive insight into their performance and behavior. This extensive scope of work soon outstripped our GPUs' capacities. Recognizing this, we turned to the high-performance computing (HPC) platform, namely the bwUniCluster2.0. This powerful infrastructure not only met our computational needs but also introduced us to efficient job scheduling. This knowledge proved crucial in optimally harnessing the HPC environment for our benefit.

To sum up, while our personal GPUs provided an essential starting point, it was the combination of these and the advanced capabilities of the HPC that truly facilitated a holistic exploration of our models across diverse training sets.

## Increase Swap

In computational systems, the swap space, often referred to simply as "swap," serves as a virtual extension of the computer's RAM. When the physical RAM (Random Access Memory) is fully utilized, the system starts using the swap space to store temporary data that doesn't fit into the main memory. Technically, the swap space resides on the system's storage drive, making it considerably slower than RAM. However, it ensures that the system doesn't run out of memory during intensive processes.

In the context of inference in machine learning models, especially deep neural networks, a significant amount of memory is required. Inference refers to the process of using a trained model to make predictions. While training a model generally demands a higher memory due to the need to backpropagate errors and update weights, inference can still be memory-intensive, especially when dealing with large models or input data. Increasing the swap space can assist in accommodating these large memory requirements during the inference process, especially on devices with limited RAM.

In our experimentation, we endeavored to increase the swap size to enhance the inference capacity of our setup on the Jetson Nano. The Jetson Nano, while a powerful edge device, comes with its constraints, especially in terms of onboard RAM. By increasing the swap size, we aimed to leverage more memory resources for inference.

However, we encountered challenges. Boosting the swap size did provide us with more virtual memory, but the prolonged and intensive read/write operations on the Jetson Nano's storage media, combined with the inherent computational demands of inference, led to significant heat generation. Just five seconds into the inference process, the device started overheating. Unfortunately, the protective mechanisms within the Jetson Nano triggered an automatic shutdown to prevent potential damage from this excessive heat.

While increasing swap size appears to be a promising approach to enhance the inference capabilities of devices with limited RAM, it's crucial to consider the potential implications, especially thermal challenges. In our case with the Jetson Nano, while the idea was technically sound, the practical implementation resulted in overheating issues. Future endeavors in similar contexts must consider not only memory management but also efficient thermal management solutions. We thus, returned to the default swap size, in order to avoid overheating of the Jetson Nano.

## Time

Our project timeline was constrained due to overlapping academic commitments, including other projects and impending exams. Such demands often limited the time we could allocate to this particular initiative, occasionally stalling our progress.

To effectively address this challenge, especially in the critical weeks approaching the deadline, we devised a targeted strategy. The team was segmented into focused subgroups, each tasked with specific components of the project. This organizational structure was grounded in the principle of harnessing the distinct expertise of each member, ensuring tasks were overseen by those best suited for them.

In addition to this subgrouping strategy, we established a routine of weekly meetings every Friday. These were conducted either in person or online, depending on the circumstances. These sessions played a pivotal role in maintaining a cohesive project trajectory. They served as a platform to review the week's progress, identify potential roadblocks, and chart out the subsequent steps. This consistent communication ensured alignment in our objectives and helped foster a collaborative environment.

By adopting this parallelized approach complemented by regular communication, we could advance on multiple facets of the project concurrently, optimizing our productivity. An essential element of our approach was the equitable distribution of tasks, ensuring balanced workloads and preventing undue stress on any individual member.

In summary, our experience underscored the importance of strategic time management, effective team organization, and consistent communication, especially when navigating tight schedules and competing commitments.
