# HPC

- [bwUniCluster2.0](#bwunicluster20)
  - [Job Management in HPC (Slurm)](#job-management-in-hpc-slurm)
    - [Partitions](#partitions)
    - [Job submission](#job-submission)
  - [Job script template](#job-script-template)

# bwUniCluster2.0

The [bwUniCluster 2.0](https://www.scc.kit.edu/dienste/bwUniCluster_2.0.php) is a high-performance computing (HPC) system located in Karlsruhe, Germany. It serves as a vital computational resource for the academic and research community in Baden-Württemberg. Specifically designed to tackle complex scientific computations, the bwUniCluster 2.0 supports research across diverse domains, including physics, chemistry, life sciences, and engineering. This powerful computing infrastructure aids researchers in processing vast amounts of data and conducting simulations that would be challenging, if not impossible, on standard computers. Through this, the bwUniCluster 2.0 significantly accelerates the pace of scientific discovery and innovation within Baden-Württemberg.

The bwUniCluster 2.0 (hereafter shortened to simply “bwUniCluster”) provided the team resources that were necessary to compute different models for the project. Training models in these fields, especially deep neural networks, demand substantial computational power and memory. Datasets for computer vision tasks, such as image recognition or video analysis, are often massive, containing a high number of high-resolution images. Utilizing the bwUniCluster allows us to harness parallel processing capabilities, reducing training times from weeks to just days or even hours. Moreover, with the enhanced memory and storage facilities of the cluster, it's possible to train models with larger batches, improving the accuracy and robustness of the models. By leveraging the power of the bwUniCluster, we can iterate faster, and experiment with more complex architectures and models while keeping the training times low.

One such example is the training of the yolov5 models. One of our team members supplied a 2070 super for a yolov5s model that was trained for 200 epochs and it took approximately 14 hours. In contrast, the same model trained on two A100s (80GB of VRAM each) on a partition of the high the bwUniCluster trained for 300 epochs in about one hour. Our team was interested in keeping the entire project flexible enough to switch between different models and make them available for different purposes as needed (whether that may be for a next to real time performance expected from smaller models like the nano models or whether the focus was the precision which may be closer to the medium sized models). Having different models at hand could also be used to harness hybrid solutions with cloud resources and edge resources that are switched to in case connectivity prevents access to the remote servers. Leveraging the bwUniCluster in combination with a custom job template script (see next section) was what enabled our team to create numerous models for the project, eliminating or at least reducing time and cost constraints altogether.

## **Job Management in HPC (Slurm)**

Slurm is an open-source, highly scalable cluster management and job scheduling system designed for Linux clusters. It serves as a fundamental component in High Performance Computing (HPC) environments. Slurm performs the planning of pending jobs (thus minimizing idle time and maximizing utilization), orchestration of job execution as specified by the planned job queues of pending work and allocation of the requested resources at execution time.

### Partitions

Partitions within Slurm are like virtual environments, accommodating various types of workloads. Different partitions can be useful for different use cases, such as general computing, GPU-accelerated tasks, memory-intensive operations, and more. Each partition may have resource limits, priority settings, and scheduling policies. For instance, a partition optimized for GPU jobs might offer nodes equipped with GPUs, while another partition designed for memory-intensive tasks may allocate nodes with large amounts of RAM. These partitions enable efficient resource allocation and optimized performance for diverse job requirements. In the bwUniCluster, the assignment to individual partitions is at the user's discretion.

For the AISS tasks, most jobs have been configured to run in the `gpu_4_a100` partition. As the name suggests, 4 A100s are available in this partition. To decrease waiting times in the queues, only two of the 4 A100s were requested for jobs. If the queues for the A100 were full, sometimes the `gpu_4_h100`, `gpu_4` or `gpu_8` partitions were used. For testing, there are dedicated development partitions prefixed with `dev`. Here, tests can be run to validate the correct operation of job scripts. To keep waiting times low, this partition is mostly limited to job times of 30 minutes or less. Details to all partitions are available in the [bwUniCluster Wiki](https://wiki.bwhpc.de/e/BwUniCluster2.0/Batch_Queues#sbatch_-p_queue).

### Job submission

Submitting jobs in Slurm is achieved through the `sbatch` command, which allows users to define job requirements and parameters (such as the required resources for the job). The submission process involves specifying the desired partition using the `-p`/`--partition` flag in the `sbatch` command (also see the partition section above). Users additionally define other attributes such as the number of nodes, CPUs, memory, and estimated execution time. Job scripts are employed to encapsulate these requirements, alongside the commands needed to execute the job.

## Job script template

For this project, a job script template was created to optimize the creation of job scripts and reduce errors in job scheduling. As soon as the preparation of code was done, often jobs were mixed up and small details were missed. Therefore, the important attributes were summarized in a script that performs the most important task for the user and abstracts many steps. Reducing these tasks to a single script avoided mistakes and provided an easy way to configure the jobs with all configuration changes in a centralized location. Jobs are then scheduled with minimal CLI commands. The job template has 4 sections with the following responsibilities:

1. SBATCH configuration
2. Configuration variables
3. Conda setup
4. Job execution

The first section moves many of the static but required `sbatch` configuration variables into the script. Adding the argument to the CLI command overwrites the script specifications which can be useful for testing the scripts in the dev environment as the allowed times shall not exceed 30 minutes.

Configuration variables are variables that are specific to the job. In the YOLO model context, these variables were usually meant to configure the model architecture, the dataset and any other configuration specific to the framework.

Environment management is mostly a tricky task. To avoid difficulties, the script also takes care of conda. It makes sure that the [software module](https://wiki.bwhpc.de/e/BwUniCluster2.0/Software_Modules) is loaded as the job is started. Then, the correct conda environment is loaded. If the environment does not exist, it is created and dependencies are installed as specified by the user.

Lastly, the actual task of the job itself is started. In our case, this comprised of the training of various models in different frameworks.

Read more about the exact configurations in the [template script](https://git.scc.kit.edu/aiss_cv/hpc_enabled_frameworks/hpc_scripts) and in the framework specific scripts (for [yolov5](https://git.scc.kit.edu/aiss_cv/hpc_enabled_frameworks/yolov5.git) and [yolov8](https://git.scc.kit.edu/aiss_cv/hpc_enabled_frameworks/yolov8.git)).

The job templates reduce the CLI commands to simple instructions limited to the partition choice which in itself is highly dynamic based on available resources:

```bash
sbatch -p <partition_name> jobs/<job_script_name>.sh
```

The following excerpt of the job template script shows, how all configurations are made within variables in one place. The slurm specific configurations are required by slurm to be at the beginning of the script. Any job specific variables are custom and follow thereafter in the job template.

```bash
#!/bin/bash
#SBATCH --time=03:00:00
#SBATCH --mem=256gb
#SBATCH --job-name=aiss_cv
#SBATCH --gres=gpu:2
# above: comments above for sbatch configuration (can be overriden in sbatch cli command)

# Conda environment name
export ENV_NAME="my_env_name"
# Configuration variables (simplify configuration of jobs)
SOME_VARIABLE=can_be_used_here

# amount of GPU devices to use (specific to GPU usage)
AMOUNT_DEVICES=2
# Generate device IDs list
DEVICE_IDS=$(seq -s "," 0 $((AMOUNT_DEVICES-1)))

#
# CONDA SETUP
#
# ...

# Job execution
python some_script.py --some_parameter $SOME_VARIABLE --device_ids $DEVICE_IDS
```
