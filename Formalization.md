# Formalization

1. [Parts](#parts)
2. [States](#states)
3. [Used Instruction Manual](#used-instruction-manual)

In the course of our project focusing on the Lego robot, we recognized the importance of distinguishing between individual components and the overall assembly stages of the robot. To bring clarity and precision to our work, we early on made the decision to categorize the Lego elements into two distinct classifications: parts and states.

"Parts" refer to the individual pieces that make up the Lego set. These are unique, foundational building blocks. We identified a total of 16 parts, each representative of a specific piece within the Lego set.

On the other hand, "states" are the various stages of assembly of the Lego robot. As the individual parts come together in different combinations, they create specific configurations or stages of the robot's construction. We have defined a total of 15 such states, capturing the progressive steps from the initial assembly to the final, fully built robot.

By distinguishing between parts and states, we aimed to bring a structured approach to our analysis, ensuring that we could accurately identify and categorize each element and stage of the Lego robot's construction. It led to a common understanding and easy communication within the team which avoided miscommunication.

## Parts

| Image                                                       | Name              |
| ----------------------------------------------------------- | ----------------- |
| ![engine](assets/Formalization/Untitled.png)                | engine            |
| ![blue_long](assets/Formalization/Untitled%201.png)         | blue_long         |
| ![blue_short](assets/Formalization/Untitled%202.png)        | blue_short        |
| ![gray_45](assets/Formalization/Untitled%203.png)           | gray_45           |
| ![gray_90](assets/Formalization/Untitled%204.png)           | gray_90           |
| ![balck_short](assets/Formalization/Untitled%205.png)       | black_short       |
| ![gray_loop](assets/Formalization/Untitled%206.png)         | gray_loop         |
| ![black_stick_short](assets/Formalization/Untitled%207.png) | black_stick_short |
| ![gray_short](assets/Formalization/Untitled%208.png)        | gray_short        |
| ![white_45](assets/Formalization/Untitled%209.png)          | white_45          |
| ![red_shaft](assets/Formalization/Untitled%2010.png)        | red_shaft         |
| ![green_3](assets/Formalization/Untitled%2011.png)          | green_3           |
| ![wheel](assets/Formalization/Untitled%2012.png)            | wheel             |
| ![gray_long](assets/Formalization/Untitled%2013.png)        | gray_long         |
| ![gray_frame](assets/Formalization/Untitled%2014.png)       | gray_frame        |
| ![tire](assets/Formalization/Untitled%2015.png)             | tire              |

## States

| Image                                               | Name     |
| --------------------------------------------------- | -------- |
| ![state_1](assets/Formalization/Untitled%2016.png)  | state_1  |
| ![state_2](assets/Formalization/Untitled%2017.png)  | state_2  |
| ![state_3](assets/Formalization/Untitled%2018.png)  | state_3  |
| ![state_4](assets/Formalization/Untitled%2019.png)  | state_4  |
| ![state_5](assets/Formalization/Untitled%2020.png)  | state_5  |
| ![state_7](assets/Formalization/Untitled%2021.png)  | state_7  |
| ![state_9](assets/Formalization/Untitled%2022.png)  | state_9  |
| ![state_11](assets/Formalization/Untitled%2023.png) | state_11 |
| ![state_13](assets/Formalization/Untitled%2024.png) | state_13 |
| ![state_6](assets/Formalization/Untitled%2025.png)  | state_6  |
| ![state_8](assets/Formalization/Untitled%2026.png)  | state_8  |
| ![state_10](assets/Formalization/Untitled%2027.png) | state_10 |
| ![state_12](assets/Formalization/Untitled%2028.png) | state_12 |
| ![state_14](assets/Formalization/Untitled%2029.png) | state_14 |
| ![state_15](assets/Formalization/Untitled%2030.png) | state_15 |

## Used Instruction Manual

[instruction_manual.pdf](assets/Formalization/instruction_manual.pdf)
