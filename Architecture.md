# Architecture

1. [Jetson Nano](#jetson-nano)
2. [Angular Frontend](#angular-frontend)
3. [Flask Backend](#flask-backend)

![Simplified Deployment Diagram of the entire Application.](assets/Architecture/Architecture.png)

Simplified Deployment Diagram of the entire Application.

## Jetson Nano

The Jetson Nano is the backbone of our system and uses the YOLO model for image analysis. This model, trained on our custom dataset and loaded from the `best.pt` file, performs well in real-time object detection and classification.

During the inference, the Nano continuously processes video streams originating from the camera feed, to perform real time predictions via the YOLO model. Each detected object, accompanied by its associated confidence score, is subsequently prepared for transmission.

In a first prototype, communication was facilitated solely via a RESTful `http` connection. The reason for that is less implementation overhead and reduction in complexity. However, this brings a few downsides to the application. First and foremost, RESTful endpoints rely on client side requests. This means, that any state changes that originate from the server can not efficiently communicated to the frontend. Instead, the state change is fetched via polling. Polling is a technique used in server client architectures that relies on regular requests being sent from the client to the server in order to get the latest state from the server. Though this implementation worked, it also lead to issues with latency as any state change was subject to the time of polling. In combination with a smoothing strategy of a couple of inferences for state changes, the latency was at times quiet significant. Therefore, after our first prototype was running, we migrated towards WebSockets for the communication of states.

WebSockets work by creating a connection to a client. This connection is not as lightweight as with a RESTful `http` connection, however it comes with a fully duplex communication channel. Through this channel, the Nano ensures asynchronous communication with the frontend client. This methodology not only facilitates immediate data transfer, solving the aforementioned latency issue, but also optimizes concurrent operations, ensuring computational resources are allocated efficiently.

Note, that the RESTful backend that delivers the information regarding next instructions and parts needed is kept separate from the inference communication with the frontend. This design choice was consciously made and comes with a couple of advantages. The first advantage is that if needed, the work load can be taken of the Jetson Nano and be run on a regular server. Since this brings in the necessity of connectivity (internet connection), we thought of this architecture as a fully modular construct. The RESTful endpoint can be run alongside the WebSocket on the Jetson Nano if needed. One could also opt for a hybrid approach in which the frontend would communicate to the Jetson Nano that the network connection to the backend is failing. Then, the Jetson Nano could spin up the RESTful service locally and provide the required data from there. This approach would yield optimal performance for each situation. For example in the context of connected cars, this could be an interesting approach.

Additionally to the frontend, the Nano provides direct visual feedback by rendering the detected objects, encapsulated by bounding boxes, on its native display. This immediate representation is invaluable for on-site verification of object detection accuracy. Using the native screen for the Jetson Nano was a decision made in order to avoid the overhead in performance and network connection needed to transfer video feed to the client (f.e. via WebRTC). Such an implementation and impacts to the performance could be looked at more closely in the [future](https://www.notion.so/Future-Outlook-2d43d6629e9343f68dbd63bacaeccf71?pvs=21).

In summary, the integration of the YOLO model, continuous data streaming, and WebSocket communication create the solid foundation for our next to real time application.

Executed within the Jetson Nano's environment, the backend ensures an uninterrupted WebSocket server availability where other applications would rely on their connectivity to a server somewhere in a data center and may struggle with latency issues. This server is particularly advantageous for dynamic applications like our Angular frontend, allowing it to connect and disconnect seamlessly. Such seamless integration means that the frontend components can engage or disengage with the backend on-the-fly, without requiring any manual intervention or restarts of the server. This design choice emphasizes the backend's ability to function reliably in real-time and adaptive scenarios, highlighting its value in ensuring fluid and consistent user experiences.

### Logitech Webcam C920

In the development phase of our project, we initially opted for the onboard CSI Camera, primarily due to its direct compatibility and ease of integration with our system. However, as testing and application trials progressed, we observed limitations in its performance. The primary concern revolved around the image quality, which proved to be suboptimal for our application's requirements. Precise identification and analysis are subject to and benefit from sharp, high-definition imaging. This holds especially true in contexts like ours where detailed structures and components are involved.

Another recurring issue we faced was the accessibility of the CSI Camera. There were instances when the camera was not accessible, disrupting the smooth operation of our system therefore presenting another obstacle in development.

After evaluating several options, we decided to transition to the Logitech Webcam C920. This camera provides good image quality and consistency in connectivity. We noticed substantial improvements in image clarity, which improved the accuracy and efficiency of our system.

In conclusion, the switch to the Logitech Webcam C920 from the onboard CSI Camera was a necessary change that improved the performance significantly and allowing our application to perform at its best.

## Front-End

For our frontend development, we chose Angular, a versatile JavaScript framework developed by Google. Comprising HTML, TypeScript, and CSS, Angular provides a robust framework for creating dynamic web applications. While other frameworks like Vue or React might be more suitable for basic user interfaces (also have a lower learning curve), our team's proficiency with Angular made it the go-to choice for our project.

We decided against displaying the camera view with its predicted boxes on the frontend. Including this would further put load on the Jetson Nano's performance, which is already being pushed to its limits. As described in the previous section, we instead opted for displaying the camera feed on a display that directly connects to the Jetson Nano.

The frontend comes with utility-driven features. The State-Bar provides a clear depiction of the current step, enabling users to get an overview of their progress and upcoming tasks. It also conveys the confidence level of the model's current predictions, informing users about the reliability of the instructions given. Buttons are available for seamless transitions between assembly and disassembly modes, and another facilitates the management of the connection with the Jetson Nano, ensuring seamless real-time interaction.

Two principal boxes display the instructions for the current state. The first offers a visual guide with an accompanying image and written instructions for the next step. A potential enhancement is under consideration, allowing users to revisit past steps and validate their work. The adjacent box enumerates the necessary parts for the imminent step, ensuring users are always prepared.

For quick checks, two buttons are positioned at the bottom. The first performs a scan to ensure all parts for the complete assembly are at hand, acting as a pre-assembly checklist. During disassembly, it checks to confirm no parts are overlooked. The second button confirms the presence of parts specifically for the upcoming step, streamlining the process for users.

Both the state and the parts employ the CircularBuffer mechanism. This data structure consistently monitors the recent states and parts as detected by our system. The most recurring state or part in the buffer is then chosen as the "dominant" one, effectively ironing out transient prediction inconsistencies to ensure steady guidance.

The information regarding instructions, the Completeness Check, and the States Part Check is sourced directly from the Rest backend, ensuring accurate and up-to-date guidance for the users.

In essence, our frontend seamlessly melds a user-centric design with practical features, simplifying the assembly/disassembly tasks and presenting insights gained through the Jetson Nano.

![UI](assets/Architecture/UI.png)

## Backend

The AISCV CV LEGO server is a RESTful backend application designed to facilitate the assembly and disassembly of a LEGO robot using computer vision (CV). This backend is a part of our prototype, which aids users in ensuring they have the right LEGO parts at each step of the assembly or disassembly process. It is programmed using Flask and the information is stored in a SQLite Database. As suggested in the first section on this page, the backend was designed in a way that allows operation on traditional infrastructure like a remote server or on the edge device itself (the Jetson Nano).

**Functionality & Features**

1. **Instruction Endpoint**:
   - Route: `/instruction`
   - Method: `GET`
   - Parameters:
     - `state`: Represents the current state of the LEGO assembly or disassembly.
     - `assemblyMode`: Determines whether the instruction is for assembly or disassembly.
   - Response: Based on the current state and mode, the backend provides the next instruction from the database, as well as the relevant parts and quantities needed for that instruction.
2. **Full Set Check Endpoint**:
   - Route: `/full-set-check`
   - Method: `POST`
   - Body: The user provides a list of identified parts using computer vision.
   - Response: The backend verifies if the user has a complete set of LEGO parts, indicates which parts are missing, and provides the average confidence level of the identified parts.
3. **State Part Check Endpoint**:
   - Route: `/state-part-check`
   - Method: `POST`
   - Parameters:
     - `state`: Represents the current state of the LEGO assembly.
   - Body: The user provides a list of identified parts using computer vision.
   - Response: The backend checks if the user has the required parts for the current state of assembly, indicates which parts are missing, and provides the average confidence level of the identified parts.

**Backend Components & Implementation Details**:

1. **Database**: The backend utilizes an SQLite database (`lego.db`), which contains tables that map states to instructions and relevant parts.
2. **CORS**: The application uses the `CORS` module, which allows the server to handle cross-origin requests. This is useful when the frontend and backend are hosted on different domains.
3. **JSON Handling**: The server is designed to handle input and output in JSON format, making it easy to integrate with other services or applications.
4. **Error Handling**: The server gracefully handles potential errors, ensuring that meaningful responses are sent to the client even when something goes wrong.
5. **Utility Functions**:
   - `full_set_calculator()`: Calculates if the user has the full set of LEGO parts.
   - `check_parts_of_states()`: Checks if the user has the necessary parts for a specific state of assembly.
   - `convert_input_to_json()`: Converts the received input to a standard JSON format for easier processing.

**Context**:
The backend serves as an oracle for the frontend to display instruction for the current state and corresponding to the assembly mode, checks whether the LEGO set is complete or if all necessary parts for the current state are present.
